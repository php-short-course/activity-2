<?php require_once "./code.php" ?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>PHP SC Activity 2</title>
</head>
<body>
    <h4>For Loop</h4>
        <?php forLoop() ?>
    <h4>Arrays</h4>
    <?php array_push($students, 'John Smith') ?>
    <pre><?php var_dump($students); ?></pre>
    <?= count($students); ?>
    <?php array_push($students, 'Jane Smith') ?>
    <pre><?php var_dump($students); ?></pre>
    <?= count($students); ?>
    <?php array_shift($students) ?>
    <pre><?php var_dump($students); ?></pre>
    <?= count($students); ?>
</body>
</html>